FROM continuumio/miniconda3:latest

RUN apt-get update -qy
RUN apt-get install -y ffmpeg cmake
COPY cellshapy/environment.yml .
RUN conda env create -f environment.yml
